'''
Sistema de Voto Eletrónico Simples (SiVotES) 
José Saias <jsaias@uevora.pt>, 2021
'''


import cherrypy
import sys
import time
import threading

# ####################################
class ElectionService(object):
   headers= {'Content-Type':'application/x-www-form-urlencoded; charset=utf-8'}


   def __init__(self, name):
      self.VOTING_OPTIONS_FILENAME ='voting_options.txt'
      self.ELECTORS_VALIDATION_CODES_FILENAME= 'electors_validation_codes.csv'
      self.voting_options= self.load_voting_options()   # load voting options (boletim de voto)
      self.electors_codes= self.load_validation_codes() # load electors/participants validation codes
      self.ballot_form= '%s'%(time.time())              # current poll id
      self.sessions_who_voted= {}
      self.attack_prevention_address_list= {} # avoid DoS and code brute force attack
      self.election_name= name
      self.voting_mode= False
      self.total_requests= 0
      self.accepted_votes= [] # non-blank votes
      self.blank_vote_count= 0
      self.invalid_vote_requests= 0
      self.top_html= '<html><head><title>SiVotES</title></head><h2>Election: '+self.election_name+'</h2>\n <br/>'
      self.html_not_active_msg= '<div style=\"background-color: yellow; width: 50vw;\"> not active </div>'
      self.html_start_page_link= '\n<a href=\"/sivotes\">start</a>'
      self.concurrent_lock= threading.Lock()


   def show_election_results(self):
       self.accepted_votes.sort()
       results= {}  # non blanks
       for v in self.accepted_votes:
           if v not in results:
               results[v]= self.accepted_votes.count(v)
       ores= []
       for v,n in results.items():
           ores.append( (n,v) )
       ores.sort()
       ores.reverse()
       tie= False
       win_rate= 0
       max_votes=0
       for n,v in ores:
           print('\t %d - %s'%(n,v))
           if max_votes==0:
               max_votes= n
               win_rate= max_votes / len(self.accepted_votes)
           elif max_votes==n:
               tie= True
       print('\t %d blank votes'%(self.blank_vote_count))
       print('\t winner rate: %.2f, tie: %s'%( win_rate, tie))


   '''To allow extra attempts for an excluded source IP address.
   If all works... this will not be needed. 
   Still, some voters can submit without validation code.'''
   def allow_address(self, address):
       del self.attack_prevention_address_list[address]  # remove counter for that address


   '''Print event counters.'''
   def show_stats(self):
      info= ' voting: %s, accepted votes: %d, electors: %d' %(self.voting_mode,self.blank_vote_count+len(self.accepted_votes), len(self.electors_codes) )
      print(info)
      info= ' total http requests: %d, invalid requests: %d'%(self.total_requests, self.invalid_vote_requests)
      print(info)
   
   

   '''Activate the voting process.'''
   def open_voting(self):
      if len(self.accepted_votes)>0 or self.blank_vote_count>0:
         print('ERROR: cannot open again! Please restart!')
         exit(1)
      print('. opening voting...')
      self.voting_mode= True
      self.sessions_who_voted= {}

	
   '''Close the voting process.'''
   def close_voting(self):
      print('. closing voting...')
      self.voting_mode= False
      self.show_election_results()



   '''Reads the voting options from a local text file (voting_options.txt), one per line.'''
   def load_voting_options(self):
      option_list=[]
      f= open(self.VOTING_OPTIONS_FILENAME)
      for v in f:
         v=v.strip()
         if v=='':
            continue
         if v not in option_list:
             option_list.append(v)
         else:
             print('ERROR: repeated vote option: %s'%(v))
             exit(1)
      f.close()
      return option_list



   '''Reads the codes that will validate each vote. Each code is valid only once.
      The file (electors_validation_codes.csv) has one record per line: 
      e-mail;code
   '''
   def load_validation_codes(self):
      codes= {}
      f= open(self.ELECTORS_VALIDATION_CODES_FILENAME)
      for v in f:
         v= v.strip()
         if v=='':
            continue
         c= v.split(';')[1].strip()
         if len(c)<4:
             print('\t ERROR: validation code wrong format: use 4 or more characters')
             exit(1)
         codes[c]= 0
      f.close()
      return codes



# ##########################################
   @cherrypy.expose
   def index(self):
       cherrypy.session['init'] = 'A'
       self.total_requests +=1
       content= self.html_not_active_msg +'<div><br/><b>\
registered voters</b>: '+str(len(self.electors_codes))+'</div>\
<br/><b>voting options to be presented</b>: ('+str(len(self.voting_options))+' + blank) <br/>\
<pre style=\"margin-left: 20px;\">'+str( '\n'.join(self.voting_options) )+'</pre>'
       if cherrypy.session.id in self.sessions_who_voted:
          #print('\t session %s already voted!'%(cherrypy.session.id))
          content= '<div style=\"background-color: #c6ffb3; width: 50vw;\"> all done</div>'
       elif self.voting_mode: 
           form_voting_options='<input type=\"radio\" name=\"v\" value=\"0\"> Blank vote / Voto em branco<br/>\n'
           for n in range(len(self.voting_options)):
               form_voting_options+= '<input type=\"radio\" name=\"v\" value=\"%s\"> %s<br/>\n'%(1+n,self.voting_options[n])
           content='<form method=\"POST\" action=\"vote\"><b>Vote / Voto</b>:<br/>'+form_voting_options+'\
       <br/><b>Validation code / Código de validação</b>: <br/><input type=\"text\" name=\"validation_code\"> \
       <br/>\n<br/><input type=\"submit\" value=\"send / enviar\">\n\
       <input type=\"hidden\" name=\"ballot_form\" value=\"'+self.ballot_form+'\"></form>'
       # -------------       
       return self.top_html + content + '\n</html>'



# ##########################################
   @cherrypy.expose
   def vote(self, **data):
      cherrypy.session['init'] = ''
      # if the poll is not open yet
      if not self.voting_mode:
          return self.top_html +self.html_not_active_msg+self.html_start_page_link+'</html>'
      # DoS prevention / validation code brute force attack prevention ----------
      source_addr= '%s'%(cherrypy.request.remote.ip)
      addr_attempts= self.attack_prevention_address_list.get(source_addr, 0)
      if addr_attempts>8:
          print('\t WARNING: source address %s exceeded vote attempts limit'%(source_addr))
          return ''
      ########
      self.concurrent_lock.acquire()   # mutual exclusion: prevent multi-browser code reuse on possible race condition
      ########
      self.attack_prevention_address_list[source_addr] = addr_attempts+1
      # -------------------------------------------------------------------------
      self.total_requests +=1
      # 
      if cherrypy.session.id in self.sessions_who_voted:
         print('\t session %s already voted!'%(cherrypy.session.id))
         self.invalid_vote_requests +=1
         answer= 'repeated vote not allowed'
      else: # session ok, now need to present a valid voting code
         if 'validation_code' not in data:
             print('WARNING: http request hasn\'t a validation code')
             answer= 'validation code not present'
         # is the code valid?
         elif data['validation_code'] not in self.electors_codes: # not valid
            answer= 'Voting code is not valid!'
            self.invalid_vote_requests +=1
            print('\t session %s sent an invalid voting code'%(cherrypy.session.id))
         else: #   ................................ presented code is valid
            vote_index= -1
            try:
                 vote_index= int(data['v'])
            except:
                 print('\t no vote received')
                 vote_index=-2
            if 'ballot_form' not in data or data['ballot_form']!=self.ballot_form: # vote request is meant for this poll?
                print('WARNING: invalid source ballot form received from session: %s'%(cherrypy.session.id))                
                answer= 'invalid source ballot form'
                self.invalid_vote_requests +=1
            elif vote_index<0 or vote_index>len(self.voting_options):
                print('WARNING: invalid voting option or wrong ballot form received from session: %s'%(cherrypy.session.id))                
                answer= 'invalid voting option or wrong ballot form'
                self.invalid_vote_requests +=1
            else:
                # was the code used before?
                elector_status= self.electors_codes[ data['validation_code'] ]
                self.electors_codes[ data['validation_code'] ]= 1+elector_status # mark the code
                if elector_status>0: # ... used before
                    print('\tsession %s already voted elector status: %d'%(cherrypy.session.id,elector_status)) 
                    answer= 'Voting code is valid but already used! Vote not accepted!'
                    self.invalid_vote_requests +=1
                else:  # ok, regular vote
                    if vote_index==0: # blank
                        self.blank_vote_count +=1
                    else:
                        self.accepted_votes.append( self.voting_options[vote_index-1] )
                    self.sessions_who_voted[cherrypy.session.id]= True
                    answer= 'Ok, accepted vote'
      self.concurrent_lock.release()
      return self.top_html + answer+'<br>'+self.html_start_page_link+'\n</html>'



# ##########################################
def start_server(election_name, port, ssl_private_key=None, ssl_certificate=None):
   scheme= 'http'
   sa_service= ElectionService(election_name)   
   cpCONF= {'server.socket_port': port, 
          'tools.encode.encoding': 'utf-8',
          'request.show_tracebacks': False,
          'server.socket_host': '0.0.0.0', 
          'tools.sessions.on': True,
          'log.screen': False, 
          'log.access_file':'access.log', 
          'log.error_file': 'error.log' }

   cherrypy.tree.mount(sa_service,'/sivotes',{'/':{}})
   cherrypy.config.update( cpCONF )
   cherrypy.lib.sessions.init()
   if ssl_private_key!=None and ssl_certificate!=None: # SSL optional args
      scheme= 'https'
      cherrypy.server.ssl_module = 'builtin'
      cherrypy.server.ssl_private_key= ssl_private_key
      cherrypy.server.ssl_certificate= ssl_certificate
   cherrypy.engine.start()

   # console loop here
   print('. point your browser to: %s://localhost:%d/sivotes'%(scheme,port))
   print('. enter help to list commands; hit enter to update counters')
   cmd= input('> ').strip()
   while cmd!='quit':
       if cmd=='help':
           print('OPTIONS:\n\t help\n\t open    # begin the voting process\n\t close   # end the voting process\n\t allow ADDRESS # to allow extra attempts from excluded IP address\n\t exit    # finish and exit')
       elif cmd=='open':
           sa_service.open_voting()
       elif cmd=='close':
           sa_service.close_voting()
       elif cmd.startswith('allow '):
           sa_service.allow_address(cmd[6:])
       elif cmd=='quit' or cmd=='exit':
           break
       print()
       sa_service.show_stats()
       print('--------- %s'%(time.strftime("%Y-%m-%d %H:%M:%S")))
       cmd= input('> ').strip()
   # on quit
   print('. finishing')
   sa_service.show_election_results()
   cherrypy.engine.exit()
   time.sleep(1)
   exit(0)



# ##########################################
if __name__ == '__main__':        
    service_port= 8080
    election_name= 'Gondor\'s President'
    ssl_private_key=None
    ssl_certificate=None
    if len(sys.argv)>1:
        if sys.argv[1].endswith('-help'):
            print('Usage:\n\
python sivotes.py PORT ELECTION_NAME\n\
python sivotes.py PORT ELECTION_NAME SSL_PRIVATE_KEY_FILE SSL_CERTIFICATE_FILE')
            exit(0)
        # arg 1: http port
        try:
            service_port= int(sys.argv[1])
        except:
            print('ERROR: port argument must be an integer')
            exit(1)
        print('. service_port: %d'%(service_port))
        # arg 2: election name
        if len(sys.argv)>2:
            election_name= sys.argv[2]
            print('. election_name: %s'%(election_name))
            if len(sys.argv)>4:  # SSL key and certificate files
                ssl_private_key= sys.argv[3]
                ssl_certificate= sys.argv[4]
    # console loop
    start_server(election_name, service_port, ssl_private_key, ssl_certificate)



'''
  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.
'''

