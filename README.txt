﻿sivotes: Simple Electronic Voting System 

SIVOTES is a simple digital voting system thought for electoral acts where voters prefer not to be meeting face-to-face. Alternatively, participants make their choice in a synchronous act, through an online meeting. 
The system simplifies the control of the voting process for the exact meeting attendees, including repeating an election in the event of a tie or if there is a need for a second round until a majority is reached. 


System guarantees:
* secrecy: each participant’s vote will not be shown;
* effectiveness: the way in which votes are collected and counted is clear and scrutinable by analyzing the application code;
* auditability: the transparency of the process is ensured by the direct browser-application communication; the access to the source code; and the permanent monitoring of the system console, by all participants.


Operation
Before the electoral act, each participant receives a VoteCode, to be used when submitting their vote. These codes are all different, must be kept secret by their owners, and can only be used once. 
Despite being open source, the code can be presented again at the beginning of the meeting, before running the application. The meeting moderator also shows the ballot voting options and the URL to access the digital polling station. The application must be run on a server having an accessible port and public address.

The application starts by reading the submission validation codes and the ballot voting options. After the "open" command, the system becomes ready for browser access, to which it responds with a form with the ballot options and the validation code.

The application console, which will be shared with all session participants, will permanently show the number of events and the total vote count, which are updated by pressing the Enter key.
With the “close” command, the vote is closed and the results are displayed.



Console commands:
     help     - list console commands
     open    - begin the voting process; start accepting vote requests
     close    - end the voting process; no more votes will be received
     allow ADDRESS  - to allow a previously excluded IP address
     exit     -  to finish and exit the program.



Run the application:
$ python3 sivotes.py 8080 "president of a LoR country"
or
$ python3 sivotes.py  443 'Election Title'  privkey.pem  certchain.pem

The first arguments are the HTTP port and the election’s title. 
The second command activates HTTPS, with files similar to those used in NGINX SSL config.




Demonstration video:
https://youtu.be/MeFFS1DT-xc



Software requirements
* python3
* cherrypy module




Security remarks
This simple application does not want to compete with already established and robust solutions, which often expect participants to trust their software and may not be as agile for a five-minute election in an online synchronous session.
SIVOTES is a weekend coding project, usable for election processes where there is a minimum of group trust. It is not designed for critical scenarios, in which the platform itself would have to be certified.
To exclude suspicions of library or infrastructure tampering, the system can be deployed in a fresh live linux distribution, from an official iso.
If you are concerned about data privacy while in transit, HTTPS can be activated with CherryPy (please see:
https://docs.cherrypy.org/en/3.3.0/progguide/security.html). You can use the third and fourth SIVOTES arguments to indicate SSL private-key and certificate files.
The vote count is done with a common approach, which is reflected in the code, and all participants control considers the HTTP session id and the validation codes. No votes or elector IDs are written on the console nor in the log files. This is the purpose of validation codes, which can be distributed at random, to decrease ID traceability risk.



License
Mozilla Public License 2.0



Maintainer / Contact
José Saias <jsaias@uevora.pt>

